<?php get_header(); ?>
<div class="container">
	<div class="row">
		<div class="col-md-12">

	<div id="primary" class="content-area">
		<main id="main" class="site-main" role="main">

		<?php
		// Start the loop.
		while ( have_posts() ) : the_post();

		?>

<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>

	<header class="page-header">
		<?php the_title( '<h2 class="page-title"><strong>', '</strong> <small>'.get_post_meta(get_the_ID(), 'employee_detail_position', true).'</small></h2>' ); ?>
		<hr>
	</header> 

	<div class="entry-content">
 <?php
 
 if ( function_exists( 'envira_gallery' ) ) { 
	envira_gallery( get_the_ID() ); 
 } 
 ?>
		<?php the_content(); ?>
	</div><!-- .entry-content -->

	<?php edit_post_link( __( 'Edit', 'twentyfifteen' ), '<footer class="entry-footer"><span class="edit-link">', '</span></footer><!-- .entry-footer -->' ); ?>

</article><!-- #post-## -->
		

		<?php
			// If comments are open or we have at least one comment, load up the comment template.
			//if ( comments_open() || get_comments_number() ) :
				//comments_template();
			//endif;

		// End the loop.
		endwhile;
		?>

		</main><!-- .site-main -->
	</div><!-- .content-area -->

		</div>
	</div>
</div>
<?php get_footer(); ?>
