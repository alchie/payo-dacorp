<?php 
/* Template Name: Contact Us */
get_header(); ?>

<div id="primary" class="content-area">
	
	
	<div class="container contactform">
		<div class="row">
			<div class="col-md-12">
					<h2 class="page-title wow fadeIn animated"><strong>Contact</strong> Us</h2>
					<div id="alert-container"></div>
			
			</div>
			<div class="form-inputs">
				<input type="hidden" name="subject" id="contact-form-subject" value="Contact Us" />
		<input type="hidden" name="referrer" id="contact-form-referrer" value="<?php the_permalink(); ?>" />
				<div class="col-md-6">
					<div class="form-group">
						<input type="text" class="form-control" id="contact-form-name" placeholder="Enter Name">
					</div>
				</div>
				<div class="col-md-6">
					<div class="form-group">
						<input type="email" class="form-control" id="contact-form-email" placeholder="Enter Email">
					</div>
				</div>
				<div class="col-md-6">
					<div class="form-group">
						<input type="phone" class="form-control" id="contact-form-phone" placeholder="Enter Phone">
					</div>
				</div>
				<div class="col-md-6">
					<div class="form-group">
						<input type="text" class="form-control" id="contact-form-address" placeholder="Enter Address">
					</div>
				</div>
				<div class="col-md-12">
					<div class="form-group">
						<textarea class="form-control" rows="3" id="contact-form-message" placeholder="Enter Message"></textarea>
					</div>
					
					<button type="submit" class="btn btn-primary btn-lg" id="contactUsSubmit">Submit</button>
				</div>
			</div>
		</div>
	</div>
</div><!-- .content-area -->
<?php get_footer(); ?>
