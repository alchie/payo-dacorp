<?php get_header(); ?>
<div class="container">
	<div id="primary" class="content-area">
	
<div class="page-header">
	<?php $current_term = get_term( get_queried_object()->term_id, 'product_category' ); ?>
	
  <h2 class="page-title text-center"><strong><?php echo $current_term->name; ?></strong></h2>
</div>
	
	<div class="row">
	
	<div class="col-md-4 archive-list">
		<h3>Categories</h3>
		<ul>
			<?php
			$categories = get_terms('product_category');
				foreach( $categories as $category ):
				$pc_class = get_custom_termmeta($category->term_id,'product_category_classname', true);
				$active_class = '';
				if( $category->term_id ==  get_queried_object()->term_id ) {
					$active_class = 'active';
				}
			?>
			<li data-wow-delay="0.2s" class="wow fadeInLeft <?php echo $active_class; ?>"><a href="<?php echo get_term_link( $category ); ?>"><?php echo $category->name; ?></a></li>
			<?php endforeach; ?>
		</ul>
	</div>
		<div class="col-md-8">
<main id="main" class="site-main" role="main">
	<!--<div class="page-header">
	<?php $current_term = get_term( get_queried_object()->term_id, 'product_category' ); ?>
	
  <h1 class="page-title"><strong><?php echo $current_term->name; ?></strong></h1>
</div>-->

		
<div class="list-group">

		<?php
		// Start the loop.
		while ( have_posts() ) : the_post();
?>
<a href="<?php the_permalink(); ?>" class="list-group-item wow fadeInUp" data-wow-delay="0.2s">
<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
 
     	
	<header class="entry-header">
		<?php the_title( '<h4 class="entry-title list-group-item-heading">', '</h4>' ); ?>
	</header> 

	<div class="entry-content list-group-item-text">
		<?php the_excerpt(); ?>
		<?php
			wp_link_pages( array(
				'before'      => '<div class="page-links"><span class="page-links-title">' . __( 'Pages:', 'twentyfifteen' ) . '</span>',
				'after'       => '</div>',
				'link_before' => '<span>',
				'link_after'  => '</span>',
				'pagelink'    => '<span class="screen-reader-text">' . __( 'Page', 'twentyfifteen' ) . ' </span>%',
				'separator'   => '<span class="screen-reader-text">, </span>',
			) );
		?>
	</div><!-- .entry-content -->

	<?php //edit_post_link( __( 'Edit', 'twentyfifteen' ), '<footer class="entry-footer"><span class="edit-link">', '</span></footer><!-- .entry-footer -->' ); ?>

		
</article><!-- #post-## -->
</a>
<?php
		// End the loop.
		endwhile;
		?>
</div>
		</main><!-- .site-main -->
	

		</div>
	</div>
	</div><!-- .content-area -->
</div>
<?php get_footer(); ?>
