<?php

require('inc/bootstrap3-menu-walker.php');

if ( ! function_exists( 'payodacorp_setup' ) ) :

function payodacorp_setup() {

	/*
	 * Make theme available for translation.
	 * Translations can be filed in the /languages/ directory.
	 * If you're building a theme based on payodacorp, use a find and replace
	 * to change 'payodacorp' to the name of your theme in all the template files
	 */
	load_theme_textdomain( 'payodacorp', get_template_directory() . '/languages' );

	// Add default posts and comments RSS feed links to head.
	add_theme_support( 'automatic-feed-links' );

	/*
	 * Let WordPress manage the document title.
	 * By adding theme support, we declare that this theme does not use a
	 * hard-coded <title> tag in the document head, and expect WordPress to
	 * provide it for us.
	 */
	add_theme_support( 'title-tag' );

	/*
	 * Enable support for Post Thumbnails on posts and pages.
	 *
	 * See: https://codex.wordpress.org/Function_Reference/add_theme_support#Post_Thumbnails
	 */
	add_theme_support( 'post-thumbnails' );
	set_post_thumbnail_size( 825, 510, true );

	// This theme uses wp_nav_menu() in two locations.
	register_nav_menus( array(
		'primary' => __( 'Primary Menu',      'payodacorp' ),
	) );

	/*
	 * Switch default core markup for search form, comment form, and comments
	 * to output valid HTML5.
	 */
	add_theme_support( 'html5', array(
		'search-form', 'comment-form', 'comment-list', 'gallery', 'caption'
	) );

	/*
	 * Enable support for Post Formats.
	 *
	 * See: https://codex.wordpress.org/Post_Formats
	 */
	/* add_theme_support( 'post-formats', array(
		'aside', 'image', 'video', 'quote', 'link', 'gallery', 'status', 'audio', 'chat'
	) ); */

}
endif; // payodacorp_setup
add_action( 'after_setup_theme', 'payodacorp_setup' );

function payodacorp_widgets_init() {
	register_sidebar( array(
		'name'          => __( 'Widget Area', 'payodacorp' ),
		'id'            => 'sidebar-1',
		'description'   => __( 'Add widgets here to appear in your sidebar.', 'payodacorp' ),
		'before_widget' => '<aside id="%1$s" class="widget %2$s panel panel-default"><div class="panel-heading">',
		'after_widget'  => '</div></aside>',
		'before_title'  => '<h3 class="widget-title panel-title">',
		'after_title'   => '</h3></div><div class="panel-body">',
	) );
}
add_action( 'widgets_init', 'payodacorp_widgets_init' );

function payodacorp_scripts() {

	wp_enqueue_style( 'OpenSans-font', 'http://fonts.googleapis.com/css?family=Open+Sans:400,300,700');
	wp_enqueue_style( 'OpenSansCondensed-font', 'http://fonts.googleapis.com/css?family=Open+Sans+Condensed:300');
	
	// Add Bootstrap, used in the main stylesheet.
	wp_enqueue_style( 'bootstrap-css', get_template_directory_uri() . '/css/bootstrap.min.css', array(), '3.1.0' );
	wp_enqueue_script( 'bootstrap-js', get_template_directory_uri() . '/js/bootstrap.min.js', array('jquery'), '20150210' );
	//wp_enqueue_style( 'bootstrap-cdn-css', '//maxcdn.bootstrapcdn.com/bootstrap/3.3.2/css/bootstrap.min.css', array(), '3.3.2' );
	//wp_enqueue_script( 'bootstrap-cdn-js', '//maxcdn.bootstrapcdn.com/bootstrap/3.3.2/js/bootstrap.min.js', array(), '20150210' );
	
	wp_enqueue_script( 'google-maps-api', '//maps.googleapis.com/maps/api/js?key=AIzaSyCsz95GSXELp54QaQ-KqtcfSjcniNmTFT4', array(), '3.0' );
	
	// WOW  Animation
	wp_enqueue_style( 'animate-css', get_template_directory_uri() . '/css/animate.css', array(), '1.0' );
	wp_enqueue_script( 'wow-min-js', get_template_directory_uri() . '/js/wow.min.js', array('jquery'), '1.0.3' );
	
	// Load our main stylesheet.
	wp_enqueue_style( 'payodacorp', get_stylesheet_uri(), array(), '20150507.5' );

	// Load the Internet Explorer specific stylesheet.
	wp_enqueue_style( 'payodacorp-ie', get_template_directory_uri() . '/css/ie.css', array( 'payodacorp-style' ), '20141010' );
	wp_style_add_data( 'payodacorp-ie', 'conditional', 'lt IE 9' );

	// Load the Internet Explorer 7 specific stylesheet.
	wp_enqueue_style( 'payodacorp-ie7', get_template_directory_uri() . '/css/ie7.css', array( 'payodacorp-style' ), '20141010' );
	wp_style_add_data( 'payodacorp-ie7', 'conditional', 'lt IE 8' );

	if ( is_singular() && comments_open() && get_option( 'thread_comments' ) ) {
		wp_enqueue_script( 'comment-reply' );
	}

	wp_enqueue_script( 'payodacorp-script', get_template_directory_uri() . '/js/scripts.js', array( 'jquery', 'bootstrap-js' ), '20150506.2', true );

}
add_action( 'wp_enqueue_scripts', 'payodacorp_scripts' );

add_action( 'wp_ajax_contact_payo', 'contact_payo_callback' );
add_action( 'wp_ajax_nopriv_contact_payo', 'contact_payo_callback' );

function contact_payo_callback(){
 if(isset($_POST['action']) && $_POST['action'] == 'contact_payo') {
				
	$message = "Name: " . $_POST['contact_name'] . "\n";
	$message .= "Email: " . $_POST['contact_email'] . "\n";
	$message .= "Phone: " . $_POST['contact_phone'] . "\n";
	$message .= "Address: " . $_POST['contact_address'] . "\n";
	$message .= "Message: \n" . $_POST['contact_msg'] . "\n\n\n";
	$message .= "REFERRER: " . $_POST['contact_referrer'];
	
	$message = stripslashes($message);
	
	$headers = 'From: '.$_POST['contact_name'].' <'.$_POST['contact_email'].'>' . "\r\n";
	
	if( wp_mail('info@payodacorp.com', "PAYODACORP.COM : " . $_POST['contact_subject'], $message, $headers) ) {
		echo json_encode($_POST);	
	} else {
		echo json_encode(false);	
	}
	exit;
 } 
}

if ( class_exists( 'Jetpack_RelatedPosts' ) ) {
	function jetpackme_remove_rp() {
		$jprp = Jetpack_RelatedPosts::init();
		$callback = array( $jprp, 'filter_add_target_to_dom' );
		remove_filter( 'the_content', $callback, 40 );
	}
	add_filter( 'wp', 'jetpackme_remove_rp', 20 );
}


function payo_entry_meta() {
	if ( is_sticky() && is_home() && ! is_paged() )
		echo '<span class="featured-post">' . __( 'Sticky', 'hcc2015' ) . '</span>';

	if ( ! has_post_format( 'link' ) && 'post' == get_post_type() )
		payo_entry_date();

	// Translators: used between list items, there is a space after the comma.
	$categories_list = get_the_category_list( __( ', ', 'hcc2015' ) );
	if ( $categories_list ) {
		echo ' | <span class="categories-links">' . $categories_list . '</span>';
	}

	// Translators: used between list items, there is a space after the comma.
	$tag_list = get_the_tag_list( '', __( ', ', 'hcc2015' ) );
	if ( $tag_list ) {
		echo ' | <span class="tags-links">' . $tag_list . '</span>';
	}

	// Post author
	if ( 'post' == get_post_type() ) {
		printf( ' | <span class="author vcard">Posted By <a class="url fn n" href="%1$s" title="%2$s" rel="author">%3$s</a></span>',
			esc_url( get_author_posts_url( get_the_author_meta( 'ID' ) ) ),
			esc_attr( sprintf( __( 'View all posts by %s', 'hcc2015' ), get_the_author() ) ),
			get_the_author()
		);
	}
}

function payo_entry_date( $echo = true ) {
	if ( has_post_format( array( 'chat', 'status' ) ) )
		$format_prefix = _x( '%1$s on %2$s', '1: post format name. 2: date', 'twentythirteen' );
	else
		$format_prefix = '%2$s';

	$date = sprintf( '<span class="date"><a href="%1$s" title="%2$s" rel="bookmark"><time class="entry-date" datetime="%3$s">%4$s</time></a></span>',
		esc_url( get_permalink() ),
		esc_attr( sprintf( __( 'Permalink to %s', 'twentythirteen' ), the_title_attribute( 'echo=0' ) ) ),
		esc_attr( get_the_date( 'c' ) ),
		esc_html( sprintf( $format_prefix, get_post_format_string( get_post_format() ), get_the_date() ) )
	);

	if ( $echo )
		echo $date;

	return $date;
}

if ( ! function_exists( 'get_payo_gallery' ) ) {
    function get_payo_gallery( $id ) {
		
		if( class_exists('Envira_Gallery_Lite') ) {
			$envira_gallery_lite = Envira_Gallery_Lite::get_instance();
			return $envira_gallery_lite->get_gallery( $id );
		}
	   
    }
}
