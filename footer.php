
<div id="contacts-box">
	<div class="container">
		<div class="row">
			<div class="col-md-6 wow fadeInUp">
                <p class="title">Contacts</p>
                <p><strong>Telephone</strong> <span>(082) 297-0924</span></p>
				<p><strong>Fax</strong> <span>(082) 297-2796</span></p>
				<p><strong>SUN</strong> <span>+63-932-638-8389</span></p>
				<p><strong>SMART</strong> <span>+63-946-645-4441</span></p>
				<p><strong>GLOBE</strong> <span>+63-917-330-0571</span></p>
                <p><strong>Address</strong> <span>201 km. McArthur Highway, Bangkal, <br>Davao City, Philippines, 8000</span></p>
                <p><strong>E-mail</strong> <span><a href="mailto:info@payodacorp.com">info@payodacorp.com</a></span></p>
				
				<ul class="social-media">
					<li><a href="https://www.facebook.com/PayoManufacturingCorporation" class="facebook" target="_blank">Facebook</a></li>
					<li><a href="https://www.youtube.com/channel/UCEc_jingUIwn0BELTsXlrTg" class="youtube" target="_blank">Youtube</a></li>
					<li><a href="https://plus.google.com/115057056119748572547/posts" class="google" target="_blank">Google+</a></li>
					<li><a href="https://twitter.com/payonidok" class="twitter" target="_blank">Twitter</a></li>
				</ul>
			</div>
			<div class="col-md-6">
			
				<section class="content_map wow zoomIn">
                      <div class="google-map-api"> 
                        <div id="map-canvas" class="gmap"></div> 
                      </div> 
                </section>

			</div>
		</div>
	</div>
</div>

</div><!--/wrap-->

<div id="footer">
  <div class="container">
		<div class="col-md-12 wow fadeInUp">
			<p>Payo Manufacturing Corporation &copy; 2015</p>
			
			<div class="webdev">
				<a href="https://www.trokis.com/" target="_blank" title="Web Developer"><img alt="Web Developer" border="0" src="<?php echo get_template_directory_uri(); ?>/images/trokis.png" /></a>
			</div>
			
		</div>
  </div>
</div>

<script type="text/javascript"> 

(function($) {
	
          google_api_map_init(); 
function google_api_map_init(){ 
	var map; 
	var coordData = new google.maps.LatLng(parseFloat(7.059763), parseFloat(125.551479)); 
	var markCoord1 = new google.maps.LatLng(parseFloat(7.059763), parseFloat(125.551479)); 
	var markCoord2 = new google.maps.LatLng(parseFloat(7.059763), parseFloat(125.551479)); 
	var markCoord3 = new google.maps.LatLng(parseFloat(7.059763), parseFloat(125.551479)); 
	var markCoord4 = new google.maps.LatLng(parseFloat(7.059763), parseFloat(125.551479)); 
	var markCoord5 = new google.maps.LatLng(parseFloat(7.059763), parseFloat(125.551479)); 
	var marker; 
 
	var styleArray = [
    {
        "featureType": "water",
        "elementType": "geometry",
        "stylers": [
            {
                "color": "#000000"
            },
            {
                "lightness": 17
            }
        ]
    },
    {
        "featureType": "landscape",
        "elementType": "geometry",
        "stylers": [
            {
                "color": "#000000"
            },
            {
                "lightness": 20
            }
        ]
    },
    {
        "featureType": "road.highway",
        "elementType": "geometry.fill",
        "stylers": [
            {
                "color": "#000000"
            },
            {
                "lightness": 17
            }
        ]
    },
    {
        "featureType": "road.highway",
        "elementType": "geometry.stroke",
        "stylers": [
            {
                "color": "#000000"
            },
            {
                "lightness": 29
            },
            {
                "weight": 0.2
            }
        ]
    },
    {
        "featureType": "road.arterial",
        "elementType": "geometry",
        "stylers": [
            {
                "color": "#000000"
            },
            {
                "lightness": 18
            }
        ]
    },
    {
        "featureType": "road.local",
        "elementType": "geometry",
        "stylers": [
            {
                "color": "#000000"
            },
            {
                "lightness": 16
            }
        ]
    },
    {
        "featureType": "poi",
        "elementType": "geometry",
        "stylers": [
            {
                "color": "#000000"
            },
            {
                "lightness": 21
            }
        ]
    },
    {
        "elementType": "labels.text.stroke",
        "stylers": [
            {
                "visibility": "on"
            },
            {
                "color": "#000000"
            },
            {
                "lightness": 16
            }
        ]
    },
    {
        "elementType": "labels.text.fill",
        "stylers": [
            {
                "saturation": 36
            },
            {
                "color": "#000000"
            },
            {
                "lightness": 40
            }
        ]
    },
    {
        "elementType": "labels.icon",
        "stylers": [
            {
                "visibility": "off"
            }
        ]
    },
    {
        "featureType": "transit",
        "elementType": "geometry",
        "stylers": [
            {
                "color": "#000000"
            },
            {
                "lightness": 19
            }
        ]
    },
    {
        "featureType": "administrative",
        "elementType": "geometry.fill",
        "stylers": [
            {
                "color": "#000000"
            },
            {
                "lightness": 20
            }
        ]
    },
    {
        "featureType": "administrative",
        "elementType": "geometry.stroke",
        "stylers": [
            {
                "color": "#000000"
            },
            {
                "lightness": 17
            },
            {
                "weight": 1.2
            }
        ]
    }
];
             
            var markerIcon = { 
                url: "<?php echo get_template_directory_uri(); ?>/images/gmap_marker.png", 
                size: new google.maps.Size(114, 114), 
                origin: new google.maps.Point(0,0), 
                anchor: new google.maps.Point(21, 70) 
            }; 
            function initialize() { 
              var mapOptions = { 
                zoom: 14, 
                center: coordData, 
                scrollwheel: false, 
                styles: styleArray 
              } 
 
              var contentString = "<div></div>"; 
              var infowindow = new google.maps.InfoWindow({ 
                content: contentString, 
                maxWidth: 200 
              }); 
               
              var map = new google.maps.Map(document.getElementById("map-canvas"), mapOptions); 
              marker = new google.maps.Marker({ 
                map:map, 
                position: markCoord1, 
                icon: markerIcon
              }); 
              
               var contentString = '<div id="content">'+
                '<div id="siteNotice">'+
                '</div>'+
                '<div id="bodyContent">'+
                '<p>201 Km. McArthur Highway, Bangkal, Davao City, Philippines, 8000 <span>TEL: (082) 297-0924 <br> FAX: (082) 297-2796</span></p>'+
                '</div>'+
                '</div>';

            var infowindow = new google.maps.InfoWindow({
                content: contentString
            });


            google.maps.event.addListener(marker, 'click', function() {
              infowindow.open(map,marker);
              $('.gm-style-iw').parent().parent().addClass("gm-wrapper");
            });


            google.maps.event.addDomListener(window, 'resize', function() {

              map.setCenter(coordData);

              var center = map.getCenter();
            });
          }

            google.maps.event.addDomListener(window, "load", initialize); 

          }  


 new WOW().init();

})(jQuery);		  
</script>


<!--Start of Zopim Live Chat Script-->
<script type="text/javascript">
window.$zopim||(function(d,s){var z=$zopim=function(c){z._.push(c)},$=z.s=
d.createElement(s),e=d.getElementsByTagName(s)[0];z.set=function(o){z.set.
_.push(o)};z._=[];z.set._=[];$.async=!0;$.setAttribute("charset","utf-8");
$.src="//v2.zopim.com/?3CMeYHGcfqlHTCY06KOAUvmJk6aHpWM5";z.t=+new Date;$.
type="text/javascript";e.parentNode.insertBefore($,e)})(document,"script");
</script>
<!--End of Zopim Live Chat Script-->

<?php wp_footer(); ?>

</body>
</html>
