<?php 
/* Template Name: Homepage */
get_header(); ?>

<?php if ( function_exists( 'get_payo_gallery' ) ) { 
 	$sliders = get_payo_gallery( get_the_ID() ); 
		
	if( $sliders['gallery'] ) {
		
 ?>
<div id="carousel-example-generic" class="carousel slide wow zoomInUp hidden-xs" data-wow-delay="1s" data-ride="carousel">
  <!-- Indicators -->
  <ol class="carousel-indicators">
	<?php 
	$indicator = 0;
	foreach( $sliders['gallery'] as $slider_img ) { ?>
    <li data-target="#carousel-example-generic" class="<?php echo ($indicator==0) ? 'active':''; ?>" data-slide-to="<?php echo $indicator++; ?>"></li>
	<?php } ?>
  </ol>


  
  <!-- Wrapper for slides -->
  <div class="carousel-inner">
    
	<?php 
	$indicator2 = 0;
	foreach( $sliders['gallery'] as $slider_img ) {  ?>
	<div class="item <?php echo ($indicator2==0) ? 'active':''; ?>">
      <?php if( $slider_img['link'] != '' ) { ?><a href="<?php echo $slider_img['link']; ?>"><?php } ?>
		<img src="<?php echo $slider_img['src']; ?>" alt="<?php echo $slider_img['alt']; ?>">
	  <?php if( $slider_img['link'] != '' ) { ?></a><?php } ?>
      <div class="carousel-caption">
        <?php echo $slider_img['title']; ?>
      </div>
    </div>
	<?php $indicator2++; 
	} ?>
     
	 
	
  </div>

  <!-- Controls -->
  <a class="left carousel-control" href="#carousel-example-generic" data-slide="prev">
    <span class="glyphicon glyphicon-chevron-left"></span>
  </a>
  <a class="right carousel-control" href="#carousel-example-generic" data-slide="next">
    <span class="glyphicon glyphicon-chevron-right"></span>
  </a>
</div>
<?php } ?>
<?php } ?>
<div id="primary" class="content-area">
<div class="container">
		<div class="row">

<?php
	$categories = get_terms('product_category');
	foreach( $categories as $category ):
		$pc_class = get_custom_termmeta($category->term_id,'product_category_classname', true);
		$pc_background = get_custom_termmeta($category->term_id,'product_category_background', true);

?>
		<div class="col-md-6 wow zoomIn">
			<div class="panel panel-default">
				<div class="homepage-category <?php echo ($pc_class !== null) ? $pc_class : 'default'; ?> <?php echo $category->slug; ?>" <?php if($pc_background != '') { ?>style="background:url(<?php echo $pc_background; ?>) left top no-repeat;" <?php } ?>>
				  <div class="gray"></div>
				  <div class="orange"></div>
				  <div class="panel-heading">
					<h3 class="panel-title"><a href="<?php echo get_term_link( $category ); ?>"><?php echo $category->name; ?></a></h3>
				  </div>
			  </div>
			</div>
		</div>
<?php endforeach; ?>
	</div>
	</div>
</div><!-- .content-area -->
<?php get_footer(); ?>
