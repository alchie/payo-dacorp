<?php get_header(); 
$show_gallery = (get_post_meta(get_the_ID(), 'products_options_hide_gallery', true) !== 'on') ? true : false;
$show_video = (get_post_meta(get_the_ID(), 'products_youtube_id', true) !== '') ? true : false;
$show_manual = (get_post_meta(get_the_ID(), 'products_manual', true) !== '') ? true : false;
?>
<div class="container">
	<div class="row">
		<div class="col-md-12">

	<div id="primary" class="content-area">
		<main id="main" class="site-main" role="main">

		<?php
		// Start the loop.
		while ( have_posts() ) : the_post();
		?>
<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>

<header class="entry-header page-header">
		<?php the_title( '<h2 class="entry-title page-title text-center wow zoomIn" data-wow-delay="1s"><strong>', '</strong></h1>' ); ?>
</header> 

<div class="row">
<?php if( $show_gallery || $show_video ) { ?>
	<div class="col-md-6">
<?php 
if ( $show_video ) { 
 ?>
 <div class="video-container">
         <iframe src="http://www.youtube.com/embed/<?php echo get_post_meta(get_the_ID(), 'products_youtube_id', true); ?>?rel=0&amp;controls=0&amp;showinfo=0" frameborder="0" width="560" height="315"></iframe>
</div>
 <?php
 } 
 
 if ( $show_gallery && function_exists( 'envira_gallery' ) ) { 
 
	envira_gallery( get_the_ID() ); 
 
 } 
 
 ?>
	</div>
	<div class="col-md-6">
<?php } else { ?>
	<div class="col-md-12">
<?php } ?>
<div class="entry-content list-group-item-text">
		<?php the_content(); ?>
<?php if ( $show_manual ) { ?>
		<a href="<?php echo get_post_meta(get_the_ID(), 'products_manual', true); ?>" class="btn btn-success btn-lg" target="_blank"><span class="glyphicon glyphicon-book"></span> DOWNLOAD MANUAL</a>
<?php } ?>
		<?php
			wp_link_pages( array(
				'before'      => '<div class="page-links"><span class="page-links-title">' . __( 'Pages:', 'twentyfifteen' ) . '</span>',
				'after'       => '</div>',
				'link_before' => '<span>',
				'link_after'  => '</span>',
				'pagelink'    => '<span class="screen-reader-text">' . __( 'Page', 'twentyfifteen' ) . ' </span>%',
				'separator'   => '<span class="screen-reader-text">, </span>',
			) );
		?>
	</div><!-- .entry-content -->
	</div>
	</div>

	
	
<div class="contactform">

		<div class="row">
			<div class="col-md-12">
				<div class="page-header">
					<h2 class="sub-title wow slideInLeft" data-wow-delay="1s">Ask for <strong>Quotation</strong></h2>
				</div>
	<div id="alert-container"></div>
		
			</div>
			<div class="form-inputs">
				<input type="hidden" name="subject" id="contact-form-subject" value="<?php the_title( 'Quotation Request - ', '' ); ?>" />
		<input type="hidden" name="referrer" id="contact-form-referrer" value="<?php the_permalink(); ?>" />
				<div class="col-md-6">
					<div class="form-group">
						<input type="text" class="form-control" id="contact-form-name" placeholder="Enter Name">
					</div>
				</div>
				<div class="col-md-6">
					<div class="form-group">
						<input type="email" class="form-control" id="contact-form-email" placeholder="Enter Email">
					</div>
				</div>
				<div class="col-md-6">
					<div class="form-group">
						<input type="phone" class="form-control" id="contact-form-phone" placeholder="Enter Phone">
					</div>
				</div>
				<div class="col-md-6">
						<div class="form-group">
							<input type="text" class="form-control" id="contact-form-address" placeholder="Enter Address">
						</div>
					</div>
				<div class="col-md-12">
					<div class="form-group">
						<textarea class="form-control" rows="3" id="contact-form-message" placeholder="Enter Message"></textarea>
					</div>
					
					<button type="submit" class="btn btn-primary btn-lg" id="contactUsSubmit">Submit Request</button>
				</div>
			</div>
		</div>
	</div>

</article>

<?php edit_post_link( __( 'Edit', 'twentyfifteen' ), '<footer class="entry-footer"><span class="edit-link">', '</span></footer><!-- .entry-footer -->' ); ?>


		<?php
		// End the loop.
		endwhile;
		?>

		</main><!-- .site-main -->
	</div><!-- .content-area -->

		</div>
	</div>
</div>
<?php get_footer(); ?>
