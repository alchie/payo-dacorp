
		
<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>

	<header class="page-header">
		<a href="<?php the_permalink(); ?>"><?php the_title( '<h1 class="page-title">', '</h1>' ); ?></a>
		<div class="entry-meta">
			<?php payo_entry_meta(); ?>
			<?php edit_post_link( __( 'Edit', 'twentythirteen' ), ' | <span class="edit-link">', '</span>' ); ?>
		</div><!-- .entry-meta -->
	</header> 

	<div class="entry-content">
		<?php the_content('READ MORE <span class="glyphicon glyphicon-play yellow"></span>'); ?>
	</div><!-- .entry-content -->
	
	
</article><!-- #post-## -->
		
<hr>
