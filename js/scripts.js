(function($) {
	
	$(document).ready(function(){ /* affix the navbar after scroll below header */
		$('#nav').affix({
			  offset: {
				top: $('header').height()-$('#nav').height()
			  }
		});	

		/* highlight the top nav as scrolling occurs */
		$('body').scrollspy({ target: '#nav' })

		/* smooth scrolling for scroll to top */
		$('.scroll-top').click(function(){
		  $('body,html').animate({scrollTop:0},1000);
		})

		/* smooth scrolling for nav sections */
		$('#nav .navbar-nav li>a').click(function(){
		  var link = $(this).attr('href');
		  var posElm = $(link);
		  if( posElm.length !== 0 ) {
			var posi = $(link).offset().top + 20;
			$('body,html').animate({scrollTop:posi},700);
		  }
		})
	});
	
	$('#contactUsSubmit').click(function() {
		var proceed = true;
		
		var isEmail = function(email) {
		  var regex = /^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/;
		  return regex.test(email);
		};
		
		var contact_name = $('#contact-form-name');
		if( contact_name.val() == '' ) {
			contact_name.closest('.form-group').addClass('has-error');
			proceed = false;
		} else {
			contact_name.closest('.form-group').removeClass('has-error');
		}
		
		var contact_email = $('#contact-form-email');
		if( contact_email.val() == '' ) {
			contact_email.closest('.form-group').addClass('has-error');
			proceed = false;
		} else {
			contact_email.closest('.form-group').removeClass('has-error');
			if( isEmail( contact_email.val() ) === false ) {
				contact_email.closest('.form-group').addClass('has-error');
				proceed = false;
			} else {
				contact_email.closest('.form-group').removeClass('has-error');
			}
		}
		
		var contact_phone = $('#contact-form-phone');
		if( contact_phone.val() == '' ) {
			contact_phone.closest('.form-group').addClass('has-error');
			proceed = false;
		} else {
			contact_phone.closest('.form-group').removeClass('has-error');
		}
		
		var contact_address = $('#contact-form-address');
		if( contact_address.val() == '' ) {
			contact_address.closest('.form-group').addClass('has-error');
			proceed = false;
		} else {
			contact_address.closest('.form-group').removeClass('has-error');
		}
		
		var contact_msg = $('#contact-form-message');
		if( contact_msg.val() == '' ) {
			contact_msg.closest('.form-group').addClass('has-error');
			proceed = false;
		} else {
			contact_msg.closest('.form-group').removeClass('has-error');
		}
		
		var contact_subject = $('#contact-form-subject');
		var contact_referrer = $('#contact-form-referrer');
		
		if( proceed ) {
			var postData = {
				action : 'contact_payo',
				contact_name : contact_name.val(),
				contact_email : contact_email.val(),
				contact_phone : contact_phone.val(),
				contact_address : contact_address.val(),
				contact_msg : contact_msg.val(),
				contact_subject : contact_subject.val(),
				contact_referrer : contact_referrer.val(),
			};
			
			$('#alert-container .alert').slideUp(function(){
				$(this).remove();
			});
			
			contact_name.prop('disabled', true);
			contact_email.prop('disabled', true);
			contact_phone.prop('disabled', true);
			contact_address.prop('disabled', true);
			contact_msg.prop('disabled', true);
			
			$.post('/wp-admin/admin-ajax.php', postData, function(response) {
				
				if( response.action == 'contact_payo' ) {
					var alert = $('<div/>').addClass('alert alert-success alert-dismissable');
					alert.html('<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>\
					<strong>Message Sent!</strong> We will contact you as soon as possible.');
					alert.appendTo( $('#alert-container') );
					$('.contactform .form-inputs').slideUp();
				} else {
					var alert = $('<div/>').addClass('alert alert-danger alert-dismissable');
					alert.html('<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>\
					<strong>SERVER ERROR!</strong> Message not sent! Please try again!');
					alert.appendTo( $('#alert-container') );
					
				}
				
				contact_name.prop('disabled', false);
				contact_email.prop('disabled', false);
				contact_phone.prop('disabled', false);
				contact_address.prop('disabled', false);
				contact_msg.prop('disabled', false);
				
			}, 'json').fail(function(xhr, textStatus, errorThrown){
				console.log( xhr.responseText );
			});
		} 
	});
	
})(jQuery);