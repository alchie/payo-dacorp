<?php 
/* Template Name: Employees */
get_header(); 
?>
<div class="container">
	<div id="primary" class="content-area">
	<main id="main" class="site-main" role="main">
		<div class="page-header">
		  <h2 class="page-title text-center"><strong>Employees</strong></h2>
		</div>

	
	
	<div class="row employees">
	
<?php 
$current_page = ( $_GET[ 'pagenum' ] ) ? $_GET[ 'pagenum' ] : 1;
$term = get_term_by( 'slug', get_query_var( 'term' ), get_query_var( 'taxonomy' ) );
global $wp_query;
$args = array_merge( array( 
	'posts_per_page' => 12, 
	'post_type' => 'employees',
	'paged' => $current_page,
	's' => $keyword,
	'order' => 'ASC',
	'orderby' => 'title',
	) );

query_posts( $args );

		// Start the loop.
		while ( have_posts() ) : the_post();
		?>
			<div class="col-md-3 col-sm-4 col-xs-12 employee-item">
				<div class="thumbnail wow fadeInUp" data-wow-delay="0.2s">
				 <a href="<?php the_permalink(); ?>" class="thumbnail-link">
				 <?php if ( has_post_thumbnail() ) {
					  the_post_thumbnail(); 
					 } else {
						 ?>
						 <img src="<?php echo get_template_directory_uri(); ?>/images/no_logo.jpg">
						 <?php
					 }?>
					</a>
				  <div class="caption">
					<a href="<?php the_permalink(); ?>">
						<?php the_title( '<h4>', '</h4>' ); ?>
					</a>
					<p><?php echo get_post_meta(get_the_ID(), 'employee_detail_position', true); ?></p>
					
					<?php edit_post_link( __( 'Edit', 'twentyfifteen' ), '<footer class="entry-footer"><span class="edit-link"><small>', '</small></span></footer><!-- .entry-footer -->' ); ?>

					</div>
				</div>
			</div>
			
		<?php
		// End the loop.
		endwhile;

wp_reset_postdata();

		?>
			
	</div>
	
<?php
	 
	$total = $wp_query->max_num_pages;
	if( get_option('permalink_structure') ) {
		$page_url = get_permalink( get_page_by_path('about/employees') ) . "?";
	} else {
		$page_url = get_permalink( get_page_by_path('about/employees') ) . "&";
	}
	
	if( $total > 1 ) {
?>
	
<div class="row">
	<div class="col-md-12">

		<div class="text-center">
			<ul class="pagination">
			  <?php for( $i = 1; $i < ($total + 1); $i++ ) { 
			  
			  ?>
				<li class="<?php echo ($current_page == $i) ? 'active' : ''; ?>">
					<a href="<?php echo $page_url; ?>pagenum=<?php echo $i; ?>"><?php echo $i; ?></a>
				</li>
			  <?php } ?>	  
			</ul>
		</div>
	</div>
</div>
	<?php } 


?>
</main>
	</div><!-- .content-area -->
</div>
<?php get_footer(); ?>
