<!DOCTYPE html>
<html <?php language_attributes(); ?> class="no-js">
<head>
	<link rel="profile" href="http://www.w3.org/2005/10/profile" />
	<link rel="icon" type="image/png" href="<?php echo esc_url( get_template_directory_uri() ); ?>/images/favicon.png">
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta name="viewport" content="width=device-width">
	<link rel="profile" href="http://gmpg.org/xfn/11">
	<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">
	<!--[if lt IE 9]>
	<script src="<?php echo esc_url( get_template_directory_uri() ); ?>/js/html5.js"></script>
	<![endif]-->
	<script>(function(){document.documentElement.className='js'})();</script>
	<?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>

<div id="wrap">
  
<header id="masthead" class="site-header" role="banner">
    <div class="container">
    <div class="row">
      <div class="col-md-6 wow slideInDown" data-wow-delay="0.5s">
			<div class="site-branding">
				<?php
					if ( is_front_page() && is_home() ) : ?>
						<h1 class="site-title"><a href="<?php echo esc_url( home_url( '/' ) ); ?>" rel="home" class=""><?php bloginfo( 'name' ); ?></a></h1>
					<?php else : ?>
						<p class="site-title"><a href="<?php echo esc_url( home_url( '/' ) ); ?>" rel="home" class=""><?php bloginfo( 'name' ); ?></a></p>
					<?php endif;

					$description = get_bloginfo( 'description', 'display' );
					if ( $description || is_customize_preview() ) : ?>
						<p class="site-description"><?php echo $description; ?></p>
					<?php endif;
				?>
			</div><!-- .site-branding -->
      </div><!-- .col-sm-6 -->
	  <div class="col-md-6">
		<div class="pull-right rightbox">
			<p class="tagline wow slideInRight">"Save Time &amp; Money! <br> Entrust to PAYO Expertise"</p>
			<p class="phone wow slideInUp"><i class="glyphicon glyphicon-earphone"></i> <span>(082) 297-0924</span></p>
		</div>
	  </div><!-- .col-sm-6 -->
    </div><!-- .row -->
    </div><!-- .container -->
</header><!-- .site-header -->
  
<!-- Fixed navbar -->

<?php
$item_wrap = '<div class="container">
  <div class="navbar-header">
    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
      <span class="sr-only">Toggle navigation</span>
      <span class="icon-bar"></span>
      <span class="icon-bar"></span>
      <span class="icon-bar"></span>
    </button>
    <div class="navbar-brand visible-xs">
	<div id="more-contact-details" style="padding:0;"><i class="glyphicon glyphicon-envelope"></i> <a href="'. get_permalink( get_page_by_path('contact') ) .'">Email</a> 
	<i class="glyphicon glyphicon-earphone"></i> <a href="tel:0822970924" class="phone">(082) 297-0924</a></div>
	</div>
  </div>
  	
  <div class="collapse navbar-collapse navbar-ex1-collapse" id="bs-example-navbar-collapse-1">

	<ul id="%1$s" class="%2$s">
		%3$s
	</ul>
	
	<form action="'. get_permalink( get_page_by_path('search') ) .'" method="get" class="navbar-form navbar-right navbar-search visible-lg" role="search">
      <div class="form-group">
        <input name="keyword" type="text" class="form-control" placeholder="Search Products" value="'.$_GET['keyword'].'">
      </div>
      <button type="submit" class="btn btn-default"><i class="glyphicon glyphicon-search"></i></button>
    </form>
	
  </div>
  
      </div><!--/.container -->
';

 wp_nav_menu( array(
    'theme_location'    => 'primary',
    'container'     => 'nav',
    'container_id'      => 'primary-navbar',
    'container_class'   => 'navbar navbar-custom navbar-inverse navbar-static-top',
    'menu_class'        => 'nav navbar-nav navbar-left', 
    'echo'          => true,
    'items_wrap'        => $item_wrap,
    'depth'         => 10, 
    'walker'        => new Bootstrap3_Walker_Nav_Menu
) );
 ?>
